// Get all to do list items
fetch("https://jsonplaceholder.typicode.com/todos")
.then((response) => response.json())
.then((json) => console.log(json))

// map method
fetch("https://jsonplaceholder.typicode.com/todos/")
.then((response) => response.json())
.then((json) => {
    let todosTitles = json.map((list) => list.title)
    console.log(todosTitles);
})

// Get a to do list items
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())
.then((json) => {
    console.log(json)
    console.log(`The item "${json.title}" on the list has a status of ${json.completed}`)
})

// Create a to do list item
fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "Created To Do List Item",
        completed: false,
        userId: 1
    })
})
.then((response) => response.json())
.then((json) => console.log(json))

// Update a to do list item using PUT
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "Updated To Do List Item",
        description: "To updated the my to do list with a different data structure",
        status: "Pending",
        dateCompleted: "Pending",
        userId: 1
    })
})
.then((response) => response.json())
.then((json) => console.log(json))

// Update a to do list item using PATCH
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PATCH",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        status: "Complete",
        dateCompleted: "07/09/21"
    })
})
.then((response) => response.json())
.then((json) => console.log(json))

// Delete an item
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "DELETE",
})
.then((response) => response.json())
.then((json) => console.log(json))